---
title: "#15 Sepia and App Updates"
author: Felix
date: 2021-10-22
tags: ["libadwaita", "health", "blanket", "solanum", "shortwave", "dialect", "gtk-rs", "metronome", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 15 to October 22.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> A large stylesheet refactoring has landed in libadwaita, light and dark variants are now shared with all their differences exported as public variables and customizable by apps. This allows to do things such as reliably recoloring the whole application into sepia.
> ![](cb0628dd650c4721f2b57c951f0e430984bec98a.png)

# Circle Apps and Libraries

### Solanum [↗](https://gitlab.gnome.org/World/Solanum)

Balance working time and break time.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Solanum 3.0.0 is out and available on Flathub, with new preferences for timer length and updated translations.
> ![](18d2bb14278fb1d877fc229e71d28242ab6f9316.png)

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 25000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> I have revamped the Shortwave station details dialog, it now includes more information, and shows the location on a map for some stations. It uses [libshumate](https://wiki.gnome.org/Projects/libshumate) for the map widget. 
> 
> The search has been improved and now offers a possibility to filter the search results by different criteria.
> ![](ZmaWbNcRioeayqHfGqeWvBXc.png)
> {{< video src="aycMQaMtapBoYYKyHMSlGciM.mp4" >}}

### Health [↗](https://gitlab.gnome.org/World/Health)

Collect, store and visualise metrics about yourself.

[Cogitri](https://matrix.to/#/@cogitri:cogitri.dev) says

> Health 0.93.0 has been released and should be available on Flathub soon. The new release of Health features a reworked main view, a new calories view a daemon to remind users to reach their stepgoal and an updated stylesheet (thanks to libadwaita). Additionally, Health's icons have been updated to be thinner and many translations have been added.
> ![](ewjpVsofeIDUKqHimDFkBqSy.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) reports

> I've added another [chapter](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_app_2.html) to the gtk4-rs book. This one extends the To-Do app so that it can filter tasks and retain them between sessions.
> 
> I've also extended the [chapter](https://gtk-rs.org/gtk4-rs/stable/latest/book/gobject_values.html) about generic values with an explanation for variants.
> 
> The chapters were reviewed by slomo, Ivan Molodetskikh and Sabrina.
> {{< video src="78531ea151b4350910fe5319231be628f2e5e79c.webm" >}}

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Dialect 1.4.0 is out and available at [Flathub](https://flathub.org/apps/details/com.github.gi_lom.dialect). It comes with localized language names, keyboard shortcuts for most actions and bug fixes.

> Mufeed Ali and I have ported Dialect to GTK4 + libadwaita.
> ![](jbrSIboIDSAzeLutanFaMcho.png)

### Blanket [↗](https://github.com/rafaelmardojai/blanket)

Improve focus and increase your productivity by listening to different sounds.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) says

> Chris 🌱️'s port of Blanket to GTK4 + libadwaita was merged!
> ![](FgTxYNcuSMGPTzHeMGMhEsHV.png)

### Metronome [↗](https://gitlab.gnome.org/aplazas/metronome)

Practice music with a regular tempo.

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) reports

> Metronome can now be translated [on Damned Lies](https://l10n.gnome.org/module/metronome/).

# Third Party Projects

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> The first release of [Junction, an application/browser chooser](https://github.com/sonnyp/Junction) is out.
> Set Junction as the default application for a resource and let it do the rest. Junction will pop up and offer multiple options to handle it.
> ![](maDZMOFZegYadyicNGsrMiCj.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) says

> Not only is Fractal’s Julian featured in [this week’s Matrix Live](https://www.youtube.com/watch?v=6Az0E2YG3z0), he also has been very active in the month since [our previous report](https://thisweek.gnome.org/posts/2021/09/twig-10/#fractal). I won’t list here the very long list of merged merge requests, but the most noteworthy changes he brought are:
> 
> * new [device list and authentication dialog](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/835)
> * new [room creation dialog](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/841)
> * fixed [session handling](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/852) and implemented [session logout](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/857)
> * improved [the way errors are displayed](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/861)
> 
> Contributions from others include [enterprisey fixing the backtrace setup](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/807) and [simplyfying some error management code](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/840), [Marco Melorio adding a separator in the UI to adapt to a change in libadwaita](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/850), [Rachit Keerti Das fixing an incorrect link to install from Flathub](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/859), and [Maximiliano cleaning up our dependencies](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/847) and [application name in code](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/845/diffs).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
