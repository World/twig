---
title: "#39 Sending Locations"
author: Felix
date: 2022-04-15
tags: ["amberol", "phosh", "boatswain", "share-preview", "cawbird", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 08 to April 15.<!--more-->

# Third Party Projects

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> I have added static location events support to Fractal.
> ![](7d7e1c4917faa8c8f9d0a3758467a9c50b77eb9d.png)

### Mousai

[SeaDve](https://matrix.to/#/@sedve:matrix.org) announces

> [Mousai](https://github.com/SeaDve/Mousai) now has a new UI and has been ported into the newer version of libadwaita.
> ![](HTZdEbmVJXHGwkSurbqcxLAl.png)

### Furtherance

[rickykresslein](https://matrix.to/#/@rickykresslein:matrix.org) reports

> [Furtherance](https://furtherance.app) 1.1.1 was released and it has a new icon! Total daily time sums are now displayed (with the option to turn them off) and there were many bug fixes. It has also been translated into German, Spanish, and Italian.
> ![](AlMYXjgnTauUDglxrnTNivjR.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> [Workbench](https://github.com/sonnyp/Workbench) gained a Library of demos/examples - there are just a couples for now - contributions welcome. Making demos is a great way to get started with GTK/GNOME development, build something cool with Workbench and open an issue.
> ![](HFTvJNWQsdnkhxXdgaXYMTpQ.png)
> ![](ifoNwDcinAhanKUHNTvChryv.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) announces

> We added swipe gestures to [phosh](https://gitlab.gnome.org/World/Phosh/phosh)'s top and home bar and moved the settings menu closer to the designs:
> {{< video src="jfeIfJFLWyvtfPuWwFgWmYrC.mp4" >}}

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) says

> There were some new additions to the libadwaita rewrite of the Twitter client Cawbird, with the addition of the Account system, the ability to load the timeline of an user and a rework of the media display.
> ![](egmJsKNCNeoCmOruwQVXbeBB.jpg)
> ![](UZczemEbGrhChlVKtxQIpWDn.jpg)
> ![](suVmyBfbrQbNskClodyFEWeG.png)

### Boatswain [↗](https://gitlab.gnome.org/World/boatswain/)

A guiding hand when navigating through streams.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Since I announced Boatswain, an app to control Stream Decks, quite a few new features and improvements found their way into the app:
> 
> * Boatswain now works well with all known models of Stream Deck (Original gen1 and gen2, Mini, XL, and MK.2)
> * MPRIS integration, with which it is possible to control the currently active media player
> * If you have multiple Stream Decks available, it is now possible to use one Stream Deck to switch the profile of other Stream Deck
> * More integration with OBS Studio by introducing actions to mute & unmute audio sources, and show & hide other sources
> 
> [I recently wrote a more detailed article about it](https://feaneron.com/2022/04/13/updates-on-boatswain/).

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> I have released Amberol 0.3.0, the latest development snapshot of the music player with no delusions of grandeur. Amberol just plays your local music, without trying to manage your music collection, edit the metadata of your music files, or build playlists. You can download the latest version from [Flathub](https://flathub.org/apps/details/io.bassi.Amberol).
> ![](0f65bb52c8bf4635f666088847cb53c455e5c948.png)

# Documentation

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) says

> [libsoup](https://gitlab.gnome.org/GNOME/libsoup) documentation has been ported to gi-docgen, you can read the new docs [here](https://www.libsoup.org/libsoup-3.0/) 🚀
> ![](0d6bf077768b15fa8d2879734eaab1bde78c2f57.png)

# GNOME Shell Extensions

[aunetx](https://matrix.to/#/@aunetx:matrix.org) says

> [Blur my Shell](https://extensions.gnome.org/extension/3193/blur-my-shell/) has been updated and mostly rewrote for GNOME 42. It now uses libadwaita for its (beautiful and modern) preferences, and is more customizable!
> 
> It also has a more uniform transparent theme for the overview components, such as the dash or the applications folder; and you can also blur the window selector in the new screenshot tool.
> Last but not least, you can now blur your applications, although this is... quite buggy (and beta), but fun!
> ![](IAVUBwCCHCOiiPMlWQawfQWr.png)

# Miscellaneous

[David Heidelberg](https://matrix.to/#/@okias:matrix.org) says

> Hey all, this week we have working flatpak runners AARCH64 (relevant for PINE64 products - PinePhone etc., Librem 5, postmarketOS phones, also few desktop machines). Kudos goes to me for HW and Bartłomiej Piotrowski for doing all the work and wiring it up :) Feel free to integrate them into your nightly builds workflows and possibly hopefully into CI templates https://gitlab.gnome.org/GNOME/citemplates/-/issues/11

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

