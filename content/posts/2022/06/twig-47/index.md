---
title: "#47 Counting Items"
author: Felix
date: 2022-06-10
tags: ["furtherance", "authenticator", "gnome-software", "workbench", "phosh", "glib"]
categories: ["twig"]
draft: false
--- 

Update on what happened across the GNOME project in the week from June 03 to June 10.<!--more-->

# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Benjamin Otte has just added a [`GListStore:n-items` property](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2738) to GLib, to make it easier to bind UI elements to whether a list store is empty

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> István Derda has [added an `--uninstall` command line option](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1389) to gnome-software, to allow starting the uninstall process for an app from the command line. This should allow easier integration of gnome-software into other things.

# Circle Apps and Libraries

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week, Amberol joined GNOME Circle. Amberol just plays your music folders and files. Congratulations!
> ![](0b06010a878ed09534c7af692d5586ec7d678773.png)

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [Authenticator](https://flathub.org/apps/details/com.belmoussaoui.Authenticator) 4.1.6 is out. The new version includes:
> 
> * Google Authenticator restore support by Julia
> * Disabled GTK Inspector in release builds
> * Redesigned account details QR code
> ![](1188f491dfe748626287a515283ed64fcfd6218c.png)
> {{< video src="162a7f48dad7304717031b27aec36459a2f924e9.webm" >}}

# Third Party Projects

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) announces

> I've released blueprint-compiler v0.2.0, the first tagged release! If you're using blueprint in a project, I highly recommend using this tag instead of the main branch, to avoid any breakage as the language develops toward 1.0. You can do this with `"tag": "v0.2.0"` in your flatpak manifest and `revision = v0.2.0` in blueprint-compiler.wrap.

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> A new version of Workbench, the sandbox to learn and prototype with GNOME technologies, is out. Here are the highlights:
> 
> * Add Blueprint markup syntax for UI
> * Add Vala programming language for Code
> * Add support for previewing templates
> * Add support for previewing signal handlers
> * Include all icons from icon-development-kit
> * Improve application design
> * Distribute Library examples under CC0-1.0
> * Respect system preference for color scheme
> * Add proper light/dark color schemes for Console
> * Fix error when importing files
> 
> https://beta.flathub.org/apps/details/re.sonny.Workbench
> ![](TfeHMoclCYunsTXlevmIvxcH.gif)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) says

> [phoc 0.20.0](https://gitlab.gnome.org/World/Phosh/phoc/-/tags/v0.20.0) and [phosh 0.20.0.beta1](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.20.0_beta1) are out adding swipe gesture support for top and bottom bar, reworked quick settings (which are now also accessible on the lock screen), a switch to latest wlroots (0.15.1) and much more.
> ![](ZCnZrRKlEDWpLQbsZrscCfKV.png)

### Furtherance [↗](https://github.com/lakoliu/Furtherance)

Track your time without being tracked

[Ricky Kresslein](https://matrix.to/#/@rickykresslein:matrix.org) says

> [Furtherance](https://github.com/lakoliu/Furtherance) v1.5.0 was released and it now has a button to repeat tasks (instead of right-click), CSV Export (thanks to Felix Zwettler), a centered timer when there are no saved tasks, and local date formats.
> ![](nqXzNAMOJXmYltISYlBVqpMP.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
