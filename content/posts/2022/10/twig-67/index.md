---
title: "#67 File Descriptors and Scopes"
author: Felix
date: 2022-10-28
tags: ["tagger", "girens", "gdm-settings", "flare", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 21 to October 28.<!--more-->

# Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Simon McVittie has just added a `g_autofd` attribute to GLib, which you can use to automatically close FDs when exiting a scope, just like `g_autofree` and `g_autoptr()` (https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3007)

# Third Party Projects

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tagger is now at V2022.10.5! This release added support for new audio file types. 
> Here's the changelog:
> * Added support for oga files
> * Added support for m4a files
> ![](tWHWKsEuNyoEPWAKktsqmGkD.png)

### Girens for Plex [↗](https://flathub.org/apps/details/nl.g4d.Girens)

Girens is a Plex Gtk client for playing movies, TV shows and music from your Plex library.

[tijder](https://matrix.to/#/@tijder:matrix.org) says

> Girens 2.0.0 is released. This is the biggest update of Girens since the first release. In this release the following things are done:
> 
> * Migrated from GTK 3 to GTK 4
> * Migrated from Libhandy to Libadwaita
> * Migrated to Blueprint for ui files
> * Improved lists for large libraries (thanks to the new Gtk4 lists)
> * Redesigned the album/artist view
> * Redesigned the show view
> * Added French and Norwegian translations
> * Improved the windowed view
> * A lot of bug fixes
>  
> Also this week I added support for [weblate translation](https://hosted.weblate.org/projects/girens/girens/). If someone want's to help with translating, it is now a lot easier.
> ![](AvrGYvThcMapBRnILwZPuDIq.jpg)
> ![](pSdOtqxYYwjhcbSqRjMmZKkc.jpg)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) [v2.beta.0](https://github.com/realmazharhussain/gdm-settings/releases/tag/v2.beta.0) has been released.
> 
> ### New Features
> 
> * Power settings
> * Import/export to file
> * Enlarge welcome message shown on login
> 
> ### Bug Fixes
> 
> * "Apply Current Display Settings" feature not working on Ubuntu and similar systems
> * Some text showing up untranslated even if translation existed (fix by [Sabri Ünal](https://github.com/libreajans))
> * Some typos (fix by [Kian-Meng Ang](https://github.com/kianmeng))
> 
> ### Other Changes
> 
> * The app is now adaptive
> 
>     - Main window is adaptive
>     - Pop-up dialogs are adaptive
> * App uses the new "About" window
> * Terminal output is colored now
> 
> For full release notes visit https://github.com/realmazharhussain/gdm-settings/releases/tag/v2.beta.0.
> ![](eXkldPBOMBqQZtsvXqgSYIxb.png)
> ![](MhatPnHzkqDSEJhzuvIyFDSY.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

A unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Flare 0.5.3 was released. Compared to the last TWIG-update, Flare gained minor new features as pasting attachments into the input box, opening attachments in the default program  and notifications for incoming calls . In 0.5.3 an additional critical bug has been fixed that rendered the application useless since 26. Oct 2022 because Signal updated their certificates. I urge everyone to update such that Flare works again. I am sorry for any inconveniences that happened due to this incident and hope we can learn from it such that it does not happen again.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
