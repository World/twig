---
title: "#81 FOSDEM Weekend"
author: Felix
date: 2023-02-03
tags: ["gaphor", "loupe", "warp", "gnome-builder", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 27 to February 03.<!--more-->

# Miscellaneous

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> It's FOSDEM time! [FOSDEM](https://fosdem.org/2023/) is a free event for software developers to meet, share ideas and collaborate.
> 
> It's happening this weekend in Brussels on Saturday 04/02 and Sunday 05/02.
> 
> Find us at the GNOME booth or join the [GNOME social event](https://foundation.gnome.org/2023/02/01/gnome-at-fosdem-2023/
> ).

# GNOME Development Tools

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> Builder's project tree has been ported away from GtkTreeView to GtkListView to improve scalability and allow for more modern features going forward. This also includes support for Drag-n-Drop using the new GTK 4 APIs.
> ![](44db2d52add9ec14492d9f43b684ede36d10c4bc.png)

[hergertme](https://matrix.to/#/@hergertme:gnome.org) says

> Builder also has received updates to global search to support filtering and previewing search results.
> ![](31a8b2fae550a12635ff6a7f6dc0e5474b220648.png)

# GNOME Incubating Apps

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) announces

> Loupe has been [accepted](https://gitlab.gnome.org/Incubator/Submission/-/issues/1) into the GNOME Incubator. The GNOME [incubation process](https://gitlab.gnome.org/Incubator/Submission) is for apps that are designated to be accepted into [GNOME Core or GNOME Development Tools](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) if they reach the required maturity.
> 
> The target for Loupe is to become the new Image Viewer in GNOME Core. You can track the progress in the the [Core milestone](https://gitlab.gnome.org/Incubator/loupe/-/milestones/2).
> ![](bf66e4d98c83e3a4bd58c96ad16d400edc5344d6.png)

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) says

> While we are busy working on the core part of Loupe – the image-loading machinery – we still have a bunch of news:
> 
> * Landed a [change](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/5447) in GTK that allows us to use swipe gestures to navigate through images
> * Mostly [re-implemented](https://gitlab.gnome.org/Incubator/loupe/-/merge_requests/82) image browsing fixing many bugs and making navigating through images much smoother
> * [Added](https://gitlab.gnome.org/Incubator/loupe/-/merge_requests/96) support for moving images to the trash
> * Added tracking of changes in image directory and reloading images when changed
> * Added a bunch of missing shortcuts
> * Fixed many small bugs
> {{< video src="8b056e504ff0c17c2fa5577412f4a72467b2a17d.webm" >}}

# GNOME Circle Apps and Libraries

### Warp [↗](https://gitlab.gnome.org/World/warp)

Fast and secure file transfer.

[Fina](https://matrix.to/#/@felinira:matrix.org) announces

> Warp 0.4 has been released with QR Code support. Scan the code with any of the [supported apps](https://gitlab.gnome.org/World/warp/-/blob/main/README.md#qr-codes) and begin the transfer immediately.
> 
> Other new features: Select custom save location and seasonal progress bar colors.
> 
> We also have experimental Windows support now, see [README.md](https://gitlab.gnome.org/World/warp/-/blob/main/README.md#windows) for more details.
> ![](YUmWmEulFvNyfaNgwUOeqcNy.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) announces

> The next release of Gaphor will have full dark-mode integration, even in the diagrams. The CSS features for diagrams have been extended to allow for special styling for dark and light mode.
> {{< video src="VwvFFiBEiIWZwwdYQRTGyEEk.webm" >}}

# Third Party Projects

[Fyodor Sobolev](https://matrix.to/#/@fsobolev:matrix.org) announces

> [Cavalier](https://flathub.org/apps/details/io.github.fsobolev.Cavalier) is an audio visualizer based on CAVA. [Now](https://github.com/fsobolev/cavalier/releases/tag/2023.01.29) it has 4 drawing modes, several new options to customize interface, keyboard shortcuts for changing most of the settings on the fly, and an ability to import/export settings.
> ![](ShkgBVtWdHVkyjQakWhsmOYv.png)

[lwildberg](https://matrix.to/#/@lw64:gnome.org) says

> This week the app "Schemes", an app to create and edit syntax highlighting style-schemes for GtkSourceView written by Christian Hergert, was submitted to flathub and is now available [there](https://flathub.org/apps/details/me.hergert.Schemes). Have fun while being creative and produce beautiful new styles!
> ![](69b4e18edb5802614278f5fd6d08827aa885662d.png)

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> I'm working on a [iplan](https://github.com/iman-salmani/iplan) application for planning personal life and workflow.
> its not ready for publish but i cant stop myself.(mybe its ready for next week)
> i have warm welcome to contribution and suggestions.
> in version 1 i will focus on offline features and availability.
> ![](CWaVXAkbBwRpVNkjGIotyAoS.png)
> {{< video src="jAqwNDOIleSPkOZOxdNEjjnu.webm" >}}

[Fyodor Sobolev](https://matrix.to/#/@fsobolev:matrix.org) reports

> Hello TWIG! My first post here, I should have started writing here earlier, but better late than never 😅
> [Time Switch](https://flathub.org/apps/details/io.github.fsobolev.TimeSwitch) is an app to run tasks after a timer. In the [latest release](https://github.com/fsobolev/timeswitch/releases/tag/2023.01.28) there was added an ability to set clock time in 24h format when a timer should finish, and also a timer can now be paused.
> ![](FXLdIlTjRMuTrumbUPqMTMyE.png)

[Daniel Wood](https://matrix.to/#/@dubstar_04:matrix.org) announces

> Design, a 2D computer aided design (CAD) application for GNOME was released this week. 
> Features:
> 
> * Supports the industry standard DXF format
> * Uses common CAD workflows, commands and canvas management
> * Drawing creation and manipulation using command line or toolbar input
> * Layer management and manipulation
> * Entity interrogation and modification
> 
> Design 43-alpha2 is available from Flathub:
> https://flathub.org/apps/details/io.github.dubstar_04.design
> ![](dTDbevWncqZMDshDJAaAYmgQ.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> This week we released phosh 0.24.0 and related components for the Linux based mobile phones near you. See [here](https://honk.sigxcpu.org/con/Phosh_0_24_0_released.html) for a summary of changes.
> ![](LkcFAEhGnkPWCoqxSsYuUOxa.png)


# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

