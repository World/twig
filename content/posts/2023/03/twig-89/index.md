---
title: "#89 Steady Framerates"
author: Felix
date: 2023-03-31
tags: ["ashpd", "tubeconverter", "libadwaita", "cartridges", "mutter", "flare"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 24 to March 31.<!--more-->

# GNOME Core Apps and Libraries

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Amongst the hundreds of changes included in Mutter 44, one of them stands out: Mutter is not affected by GPU-intensive apps and games anymore, and can keep steady framerates when they're running. This fantastic improvement is in the spotlight in most recent article in the Mutter & GNOME Shell blog! [Read more about it here](https://blogs.gnome.org/shell-dev/2023/03/30/ensuring-steady-frame-rates-with-gpu-intensive-clients/).
> ![](92c2cc24de786cc91b28168b707f0c500bcd1abf.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Maksym Hazevych](https://matrix.to/#/@mks-h:matrix.org) says

> AdwPreferencesPage now has a `description` property that makes it easier to show a description for the whole preferences page.
> ![](glJzicFLdcXLZKUagdbfulmD.png)

[Alex (they/them)](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita now has [`AdwSwitchRow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.SwitchRow.html) to make the common case of having a row with a single switch easier

# GNOME Circle Apps and Libraries

### ashpd [↗](https://github.com/bilelmoussaoui/ashpd)

Rust wrapper around freedesktop portals.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> We have finally released a new stable release of [ASHPD](https://lib.rs/ashpd). The release contains a lot of API improvements making it super easy to use portals from Rust.
> 
> Code example of taking a screenshot using the new builder API
> ```rust
> use ashpd::desktop::screenshot::Screenshot;
> 
> async fn run() -> ashpd::Result<()> {
>     let response = Screenshot::request()
>         .interactive(true)
>         .modal(true)
>         .send()
>         .await?
>         .response()?;
>     println!("URI: {}", response.uri());
>     Ok(())
> }
> ```
> 
> For more details: https://github.com/bilelmoussaoui/ashpd/releases/tag/0.4.0

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> A new release of [ASHPD demo](https://flathub.org/apps/details/com.belmoussaoui.ashpd.demo) is out on Flathub! The demo comes with various nice features closing the gap of the supported portals
> 
>  - Camera: Multi camera support
>  - Notifications: Support setting Default action/action target, allow adding buttons
>  - Email: support adding attachments
>  - Make use of libadwaita widgets
>  - Fix application adaptiveness
> ![](50fcf988628a6296dd99ec3fd0e1f294effa7b34.png)

# Third Party Projects

[gregorni](https://matrix.to/#/@gregorni:hackliberty.org) announces

> This week I released my app ASCII Images on [Flathub](https://flathub.org/apps/details/io.gitlab.gregorni.ASCIIImages). It uses jp2a to turn your PNG or JPEG images into beautiful, highly detailed ASCII art.
> ![](RVsjuggUFWSxrVRknGHXyNmw.png)
> ![](hswCbbVNHGVrcnqvuLvmjkNq.png)

[Felipe Kinoshita](https://matrix.to/#/@felipekinoshita:matrix.org) says

> I just released [Telegraph](https://flathub.org/apps/details/io.github.fkinoshita.Telegraph), it's a silly little app for writing and decoding Morse code.
> ![](wojOBhMaUlLpwAichTSQUjvb.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> After a year in development, I have finally released the first version of [oo7](https://lib.rs/crates/oo7). It is a Rust library that aims to provide a libsecret replacement, tight integration with the secrets portal and a way for apps to migrate their secrets from the host to the sandboxed keyring.
> Thanks to Sophie Herold, Maxmiliano & Daiki Ueno for their huge help shaping up the building blocks of the library!

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tube Converter [V2023.3.1](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2023.3.1) is here! This week's release features many bug fixes and the port to the GNOME 44 platform :)
> Here's the full changelog:
> * Fixed an issue where the user could not download playlists with unavailable videos
> * Fixed an issue where some videos could not be downloaded when embedding metadata
> * Fixed an issue where videos with invalid filename characters could not be downloaded
> * UX/UI improvements (including moving Adw.MessageDialog dialogs to proper Adw.Window)
> ![](pSKYTTlqGtoSmCpJbwYRZPBF.png)
> ![](KRpQAjzToZmTEWjvSLKuVXdh.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

An unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) announces

> Flare 0.7.0 was released. The biggest update for this release is certainly the new and improved UI thanks to Jan-Michael Brummer. Besides that, Flare now also integrates with feedbackd, gained support for profile names and some new small features, fixes and changes. For all changes, see the [changelog](https://gitlab.com/Schmiddiii/flare/-/blob/master/CHANGELOG.md#070-2023-03-29). Also note that updating to this release from 0.6.0 requires a relink of the device.
> ![](uhTcIrstxoQNRFpdyXxGqVVz.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) says

> This week I released Cartridges, a Libadwaita game launcher for all your games.
> The project was born out of a personal need for a launcher that let me quickly start any game from any platform with just one click. No need to manage yet another game library.
> It currently supports importing games from Steam, Heroic and Bottles with no login necessary and more sources on the way.
> Check it out on [Flathub](https://flathub.org/apps/details/hu.kramo.Cartridges)!
> ![](UdJeSGECvkRHSpwMcPINqeqE.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> This week, the GNOME Foundation has been busy organizing various events such as GUADEC 2023, LAS 2023 in collaboration with KDE, and looking for other open source and community events to participate in. As a graphic designer, much of my work for these events involves creating designs such as name badges, signs, banners, and social media content - all of which I’m currently working on! Look out for a new page on the LAS website soon. I’ll be adding graphics for people to use on their social media posts or in their presentations.
> 
> Right now in-person attendees of LAS can place an order for an event t-shirt. Orders are due today so if you’re attending in person and would like to purchase one, fill out the form here: [https://conf.linuxappsummit.org/event/5/registrations/13/](https://conf.linuxappsummit.org/event/5/registrations/13/). If you’re attending remotely we have shirts and a few additional items available online: [https://shop.spreadshirt.com/linux-app-summit/all](https://shop.spreadshirt.com/linux-app-summit/all)
> 
> Open Calls for Participation:
> We’ve extended the GUADEC 2023 call for participation, all proposals are now due by April 4th. If you have a talk, workshop, panel, or BoF you’d like to host make sure to get your submissions in soon! [https://foundation.gnome.org/2023/02/24/guadec-2023-cfp/](https://foundation.gnome.org/2023/02/24/guadec-2023-cfp/)
> 
> We’re also looking for GUADEC 2023 sponsors! If you or your company would like to sponsor this year’s conference, you can find our brochure and learn more here: [https://events.gnome.org/event/101/page/167-sponsors](https://events.gnome.org/event/101/page/167-sponsors)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

