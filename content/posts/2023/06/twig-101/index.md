---
title: "#101 Libadwaita Happenings"
author: Felix
date: 2023-06-23
tags: ["gnome-control-center", "denaro", "gnome-network-displays", "gnome-calendar", "phosh", "gnome-text-editor", "tubeconverter", "iplan", "epiphany", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 16 to June 23.<!--more-->

# GNOME Core Apps and Libraries

### Text Editor [↗](https://gitlab.gnome.org/GNOME/gnome-text-editor/)

Text Editor is a simple text editor that focus on session management.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> Text Editor is now using the libadwaita's `AdwToolbarView`. See https://blogs.gnome.org/alexm/2023/06/15/rethinking-adaptivity/ for more information about the new widget and how it can improve adaptability to different form factors.
> ![](d123dc93f3f032356a853156700b39b914e545d1.png)

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) says

> the "Edit Calendar" page in GNOME Calendar was recently [ported](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/318) to libadwaita widgets! We also added mnemonics to improve keyboard accessibility, as well as tooltips.
> ![](47e91feeed3f79a0efa7889a61fa04126a6f77cf.png)
> ![](0d3568c615f11617ff765a8474bc0078471df2cf.png)

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) announces

> GNOME Calendar now uses the new AdwNavigationSplitView widget and AdwBreakpoint for adaptivity. This means it now fits properly on mobile portrait mode - something that wasn't possible with the old libadwaita widgets. We also now use AdwMessageDialog instead of GtkMessageDialog.
> ![](5c3fdf7b5bac50879d2d9e37dd92941175bd8b7d.png)
> ![](ecf8677ec73e31086fdbbce9fb0033c04737c2ca.png)
> ![](f3711ba684d7532988731ccc33b48d1d4d0e2887.png)

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) reports

> Epiphany is using new libadwaita widgets now, as well as AdwTabOverview
> ![](86ec900a1722230a901d26dd30f8eb728b7f416c.png)
> ![](88ecac160231bf07a115a9240c4d9738bb92956e.png)

### Disks

[kailueke](https://matrix.to/#/@pothos:matrix.org) reports

> GNOME Disks is in need of contributors for the GTK4 port and the UI refresh planned in [gnome-disk-utility#274](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/issues/274) (these mockups have been around for a while). The GTK4 port and the UI refresh could be done separate but it maybe saves work if it's done at once (there could be an easy way to use GTK4 without rewriting the partition view, not sure).


# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench 44.2 is out. There are 30 new demos and examples in this release which brings the total amount of Library entries above 50. Each entry is carefully crafted to feature good coding practices, be easy to reason about and respect GNOME HIG and patterns.
> 
> <a href='https://flathub.org/apps/re.sonny.Workbench'><img width='240' alt='Download on Flathub' src='https://dl.flathub.org/assets/badges/flathub-badge-en.png'/></a>
> 
> Thanks to all contributors, specially Outreachy and GSoC students [José Hunter](https://josecodes.hashnode.dev/introducing-myself), [Akshay Warrier](https://akshaywarrier.medium.com/gsoc-2023-introductory-post-109019258bd9), [Sriyansh Shivam](https://sonichere.hashnode.dev/gsoc-2023) and [Andy Holmes](https://www.andyholmes.ca) for co-mentoring.
> 
> Here is the list of new entries
> 
> * Popover
> * Screenshot
> * Header Bar
> * Font Dialog
> * Preferences Window
> * Web View
> * Boxed Lists
> * Drag and Drop
> * Tab View
> * Drawing Area
> * Advanced Buttons
> * About Window
> * Animation
> * TextView
> * Styling with CSS
> * Spin Button
> * Banner
> * Carousel
> * Color Picker
> * Email
> * Progress Bar
> * Wallpaper
> * Drop Down
> * Video
> * Revealer
> * Account
> * Stack
> * Status Page
> * Frame
> * Account
> 
> Happy coding!
> ![](531a9c264d2f9424831f5394e3c6c56d2bfe5576.png)
> ![](07ad7d7ca581e1dfa3d6cf60effd3a74d62116bc.png)
> ![](d7785654c4a49629458bd4791f7bb70c08c247c5.png)

# Third Party Projects

[Vlad Krupinski](https://matrix.to/#/@mrvladus:matrix.org) reports

> [List](https://github.com/mrvladus/List) - simple to-do application version 44.6.4 is out!
> 
> This week updates are:
> * Added some nice animations
> * New setting for data backup
> * Added Turkish translation
> * Updated symbolic icon
> * UI improvements and bug fixes
> 
> Available on [Flathub](https://flathub.org/apps/io.github.mrvladus.List)
> ![](ASqLssQymEOdwyXRWSReJDeR.png)

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) announces

> [GDM Settings](https://gdm-settings.github.io) [v3.2](https://github.com/gdm-settings/gdm-settings/releases/tag/v3.2) was released with the fix for not being able to change background image/color on GNOME 44 and updated translations.

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter [V2023.6.2](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.6.2) is here! This release features brand new documentation provided by the `Help` menu item and includes some bug fixes when working with credentials and validating URLs.
> Here's the full changelog:
> * Added a "Help" item in the main menu to display yelp docs about Tube Converter
> * Recently completed downloads will be shown first in the completed group instead of being appended last
> * Tube Converter will now automatically select the xdg-download folder as the save folder if no previous folder is available
> * Fixed a performance issue with displaying logs when the logs were large in size 
> * Fixed an issue where the app would crash if the authentication credentials were invalid 
> * Fixed an issue where some supported URLs could not be validated 
> * Updated translations (Thanks everyone on Weblate!)
> ![](CkTEIRQlTpZbLhcegLaFdwdC.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Julian](https://matrix.to/#/@juliannfairfax:matrix.org) says

> It is my pleasure to announce that the recently released PineTab2 from PINE64 already runs Phosh, so you can use and develop your favourite GNOME applications on it!
> 
> Here's how to run it, using Mobian: https://wiki.debian.org/InstallingDebianOn/PINE64/PineTab2
> 
> Thank you to everyone who helped me make this possible!
> ![](uRvvmMJDhcNSsheljaihiFEe.jpg)
> ![](HIhgKnZclBbDFWdWYWujoZQW.jpg)

### IPlan [↗](https://github.com/iman-salmani/iplan)

Your plan for improving personal life and workflow

[Iman Salmani](https://matrix.to/#/@imansalmani:matrix.org) announces

> [IPlan 1.6.0](https://github.com/iman-salmani/iplan) is now out!
> 
> Changes this week contains:
> 
> * New design for drag and drop
> * New Project create window
> * Close windows with escape key
> * Drop task on each other
> * Scroll while dragging task
> * Rename tasks list to section
> * Czech translation updated thanks to Amerey.eu
> * Code refactoring, Bug fixes, and UI improvements
> 
> you can get it from [Flathub](https://flathub.org/apps/ir.imansalmani.IPlan).
> ![](UOBfrqgDjrcZiOgZjHXrrYos.png)

### GNOME Network Displays [↗](https://gitlab.gnome.org/GNOME/gnome-network-displays)

Stream the desktop to Wi-Fi Display capable devices

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) says

> GNOME Network Displays can now be compiled as headless daemon! All it does for the time being is printing available sinks to the terminal, but that's the foundation that will later enable shell and settings integrations.
> ![](iNGRkjxMTRLDbomEkwUDmWNU.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Denaro [V2023.6.1](https://github.com/NickvisionApps/Denaro/releases/tag/2023.6.1) is here! This release features some minor bug fixes and UX tweaks.
> 
> Here's the full changelog:
> * Added the ability to select a file to import existing data from in the new account setup dialog
> * Improved custom currency preferences design
> * Fixed an issue where the Help docs were not installed on an English system
> * Updated translations (Thanks to everyone on Weblate)!
> ![](oUUBIjlAclibJTEvMxLJHdtb.png)

# Shell Extensions

[Marcin Jahn](https://matrix.to/#/@mnjahn:matrix.org) says

> I've developed a new Gnome Shell extension - [Peek Top Bar On Fullscreen](https://github.com/marcinjahn/gnome-peek-top-bar-on-fullscreen-extension). It enables macOS-like capability of revealing Gnome top panel while full screen content is on. Just hover mouse cursor to the top of your primary screen to check the time, or to switch some quick settings toggles, without leaving full screen mode.
> ![](tEOsHNpqUxgZBnCvPJShgbdI.png)

# GUADEC 2023

[mwu](https://matrix.to/#/@mwu:gnome.org) reports

> We are getting ready for GUADEC 2023 in Riga, Latvia and we hope to see you there!  Registration is open for both in person and virtual attendance.  Please register early so that we can get an in person and virtual headcount. Why do we ask this? Its helpful to know how popular sessions are, where people are attending from and if there is interest in our events.  To register please go to https://foundation.gnome.org/2023/05/04/guadec-2023-registration-is-open/.  We are very grateful for our current sponsors:  RedHat, ARM, Codethink, OpenSUSE, Ubuntu, Centricular, and Endless who have all stepped up in supporting GUADEC 2024. We still need to raise more money and if you or your company are interested, please email sponsors@guadec.org.  Look fwd to seeing you in Latvia!

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> [GUADEC 2023](https://events.gnome.org/event/101/) is only 1 month away! We've planned two social event options for our final conference day. If you're attending in person in Riga and would like to join us for a [day trip in Northern Latvia](https://events.gnome.org/event/101/page/164-northern-latvia-trip) or a [walking tour of Old Riga](https://events.gnome.org/event/101/page/237-old-riga-walking-tour), make sure to register by June 26: https://discourse.gnome.org/t/join-us-for-guadec-2023-social-events/15764

[Asmit Malakannawar](https://matrix.to/#/@asmit2952:matrix.org) says

> Are you attending #GUADEC2023 in Riga? We’ve organized two awesome social events for our final conference day, July 31! Reserve your spot on either option by registering before June 26.
> 
> And don’t forget to register as an in-person attendee: [GUADEC 2023 (26-31 July 2023): Registration · GNOME Events (Indico)](https://events.gnome.org/event/101/registrations/51/)
> 
> Northern Latvia Day Trip
> Our first option is the Northern Latvia Day Trip!
> 
> This 8hr tour includes transport to a number of historic spots including Gauja National Park, Bīriņi Manor, and the seaside resort of Saulkrasti.
> 
> Learn more and register here: [GUADEC 2023 (26-31 July 2023): Northern Latvia Trip · GNOME Events (Indico)](https://events.gnome.org/event/101/page/164-northern-latvia-trip)
> 
> Old Riga Walking Tour
> Our second option is an Old Riga Walking Tour!
> 
> This 2hr tour walks us through the winding streets and cobblestone alleys of historic Old Riga.
> 
> Learn more and register here: [GUADEC 2023 (26-31 July 2023): Old Riga Walking Tour · GNOME Events (Indico)](https://events.gnome.org/event/101/page/237-old-riga-walking-tour)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

