---
title: "#113 Numerous Updates"
author: Felix
date: 2023-09-15
tags: ["weather-oclock", "tagger", "gnome-network-displays", "parabolic", "cavalier", "libadwaita", "denaro", "just-perfection"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 08 to September 15.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) reports

> I just released libadwaita 1.4 and wrote an overview of the changes: https://blogs.gnome.org/alicem/2023/09/15/libadwaita-1-4/

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> [Errands](https://apps.gnome.org/List/) has been accepted into GNOME Circle. This app lets you keep track of your tasks with a carefully selected collection of features, like subtasks, custom colors and a history of previously cleared tasks. Congratulations!
> ![](5d3f694f24ea38558df9e8cba84e00ad48cc6874.png)

# Third Party Projects

[dlippok](https://matrix.to/#/@dlippok:matrix.org) announces

> Since the last announcement on TWIG, the Photometric Viewer now has the ability to convert between IESNA and EULUMDAT file formats. Additionally, you can export photometric data in CSV and JSON formats, as well as light distribution curves in PNG or SVG formats. Furthermore, the software now allows you to calculate the annual power consumption of the luminaire and compare its light color temperature to that of common light sources. In addition to English, the application is also available in German and Polish.
> 
> Newest version is now available on [Flathub](https://flathub.org/apps/io.github.dlippok.photometric-viewer) and [Snap Store](https://snapcraft.io/photometric-viewer).
> ![](tOqDpdQdATrLSRCUmxNywJUi.png)
> ![](pDFgruzlaYdXmxtfzGepiGgX.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tagger [V2023.9.1](https://github.com/NickvisionApps/Tagger/releases/tag/2023.9.1) is here!
> 
> This release includes many new features and improvements to Tagger's music file system. The Music Folder has been replaced with the Music Library allowing users to not only open folders, but to open playlist files as well and manage the files referenced in the playlist all from within the app. We also added support for creating a playlist file from a folder and fixed some issues with synchronized lyrics.
> 
> Here's the full changelog:
> * Added the ability to open, manage, and create playlists within Tagger. As a result, the "Music Folder" has now become the "Music Library" that can be opened from a folder or a supported playlist file
> * Synchronized lyric timestamps will now be shown in the mm:ss.xx format as per the LRC specification. When creating a new lryic, both hh:mm:ss and mm:ss.xx can be specified and Tagger will convert them appropriately
> * Tagger will display headers in the list of music files when sorting to provide a more organized view of files
> * Fixes an issue where lrc files were not importing correctly
> * Tagger's main window size will be remembered and restored on application restart
> * Updated translations (Thanks everyone on Weblate!)
> ![](ArcBiiJHdSTxUnVSxekSbEzE.png)
> ![](BqOjsGtvyElMTLVmzfZRWJPo.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Parabolic [V2023.9.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2023.9.0) was released this week! This release includes some new feature to make Parabolic even more powerful and secure!
> 
> First, the Keyring is now backed by the system credential manager, meaning that users no longer need to provide a password to secure their Keyring as it would be handled by system secrets. Of course, we offer a guide inside the app's help on moving old password-secured keyrings to the new secret-secured ones.
> 
> Besides keyrings, we improved Parabolic's support for subtitles by downloading the correctly supported format for a file type and supporting two letter language with region codes. We also introduced a new preference for choosing to remove identifying media source information from embedded metadata.
> 
> Here's the changelog:
> 
> * Added support for two letter language with region codes
> * Added a preference for removing media source data from embedded metadata (previously it was automatically removed)
> * Parabolic will now use the system's credential manager (i.e. DBus Secret Service) for securing the Keyring. Please read the Keyring page in Help for information on migrating the old password-secured Keyring to the new secret-secured Keyring 
> * Parabolic will no longer ask for a subtitle format but instead use the format supported by the file type
> * Fixed an issue where translated metadata was embedded instead of original media metadata
> * Updated translations (Thanks everyone on Weblate!)
> ![](WHVwiYuSlbgsFUbeZENwFMGF.png)

### GNOME Network Displays [↗](https://gitlab.gnome.org/GNOME/gnome-network-displays)

Stream the desktop to Wi-Fi Display capable devices

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) says

> GNOME Network Displays can now embed your cursor in its video stream. No more guess work trying to figure out where your mouse pointer is!
> 
> PS: the glitchy vertical bars are my TV's fault. It's a an old TV!
> ![](hBBQLkLfXxHCLnnkPbDBtHsi.jpg)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Denaro [V2023.9.2](https://github.com/NickvisionApps/Denaro/releases/tag/2023.9.2) was released! This release includes many bug fixes for the new Currency Conversion tool we introduced in the last release. A huge thanks to @JoseBritto and @UrtsiSantsi who have contributed to finding these issues and creating fixes ❤️
> 
> Here's the full changelog:
> * Fixed an issue where the currency conversion service would provide the wrong conversion rate
> * Fixed an issue where small converted currency amounts would show as 0
> * Fixed incorrect display of amounts for locales that use Cape Verdean escudo as currency
> * Improved UX in Currency Converter dialog
> * Updated translations (Thanks to everyone on Weblate)!
> ![](FxWVddIySJjJzBZqlSjTWPMp.png)

### Cavalier [↗](https://flathub.org/apps/details/org.nickvision.cavalier)

Visualize audio with CAVA.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Cavalier got [V2023.9.0-beta1](https://github.com/NickvisionApps/Cavalier/releases/tag/2023.9.0-beta1) this week! 
> Combine beautiful pictures with gradients to give Cavalier unique look that fits your mood!
> 
> Here's the full changelog:
> * Added ability to set foreground image for Box modes
> * Transparency can now be set both for background and foreground image
> * Cavalier switched back to using PulseAudio by default. You can still switch audio backend to whatever is supported by CAVA using CAVALIER_INPUT_METHOD environment variable
> * Updated translations (Thanks everyone on Weblate!)
> ![](pfyNZymxAPzrHnIhHAtRsRjt.png)

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> We've updated the [EGO Review Guidelines](https://gjs.guide/extensions/review-guidelines/review-guidelines.html#gtk-and-gdk-imports) and GNOME Shell 45 extensions no longer should import Gtk and Gdk to the shell process.

### Weather O'Clock [↗](https://extensions.gnome.org/extension/5470/weather-oclock/)

Display the current weather inside the pill next to the clock.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> **Weather O'Clock** version 9 was released with support for _GNOME Shell 45_ using the new symbolic weather icons. [Check it out on EGO](https://extensions.gnome.org/extension/5470/weather-oclock/).

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) announces

> [Just Perfection Extension](https://extensions.gnome.org/extension/3843/just-perfection/) now supports GNOME Shell 45.
> In this version, we have a new animation option (almost none) that can disable GNOME Shell animation without disabling apps animation. App menu feature has been removed since there isn't any app menu indicator in GNOME Shell 45. Activities button icon feature also got removed since GNOME Shell 45 uses workspace indicators in the activities button.
> This version is named after German painter Albrecht Durer.
> https://www.youtube.com/watch?v=rgIm2ggn67I

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> It's nearing the end of September, which for the GNOME Foundation, means we are nearing the end of our Fiscal Year. It's sometimes a surprise to folks that fiscal years aren't always aligned with the calendar year, but it's common for corporations and non-profits. This always leads to a flurry of activity, between getting travel reimbursements and expenses pushed through so they are reflected this year's books, working on the books to make sure there's nothing outstanding, and looking at creating the budget for next year. There's a lot to do, so please be patient if you are expecting something from me!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

