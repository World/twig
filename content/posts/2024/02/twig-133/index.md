---
title: "#133 FOSDEM 2024"
author: Felix
date: 2024-02-02
tags: ["paper-clip", "gtk", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 26 to February 02.<!--more-->

# Events

[Cassidy James Blaede](https://matrix.to/#/@cassidyjames:gnome.org) reports

> Going to FOSDEM this weekend? Meet up with GNOME and Flathub folks!
> 
> * [GNOME booth and GNOME Beers](https://foundation.gnome.org/2024/01/29/gnome-at-fosdem-2024/)
> * [Flathub BoF and more](https://docs.flathub.org/blog/fosdem-2024)
> 
> If you're unable to make the Flathub BoF but still want to chat, you can catch the team around the GNOME and KDE stands.
> 
> See you there!

[alatiera](https://matrix.to/#/@alatiera:matrix.org) reports

> We are organizing a [GStreamer hackfest](https://hedgedoc.gnome.org/gstreamer-hackfest-spring-2024) in Thessaloniki, Greece on May 27th-29th. In addition the #9 installment of the [GNOME ♥️ Rust Hackfest](https://hedgedoc.gnome.org/hackfest-gnome-rust-2024#) series will take place in the days after 31st of May - 5th of July. For more details see the [blogpost](https://blogs.gnome.org/alatiera/2024/02/02/thessaloniki-spring-hackfests/) and the handbook [hackfests page](https://handbook.gnome.org/events/hackfests.html).
> ![](aYwHEmlGOrGmVgelAgRGQdXa.png)

# GNOME Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> The GTK developers are having a hackfest in Brussels right before the FOSDEM weekend. Lots of work on rendering, media, input, and accessibility. Look forward to a full report on the GTK development blog.

# GNOME Websites

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> The [GNOME Project Handbook](https://handbook.gnome.org/) was [officially announced](https://blogs.gnome.org/aday/2024/01/30/announcing-the-gnome-project-handbook/)! This new resource provides all the information people need to participate in the GNOME project, including how the project works, how to get accounts and use project infrastructure, the release cycle, issue management, and much more. Everyone is invited to help keep the handbook up to date and accurate.
> The handbook is part of an ongoing to effort to the retire the GNOME wiki. More announcements about this will be coming soon.

# GNOME Circle Apps and Libraries

### Paper Clip [↗](https://apps.gnome.org/PdfMetadataEditor/)

Edit PDF document metadata.

[Diego Iván M.E](https://matrix.to/#/@dimmednerd:matrix.org) says

> This week, Paper Clip v5.0 was released! This release brings a couple of quality-of-life improvements, enhancements and a shiny new feature:
> 
> * Paper Clip now supports editing encrypted documents, thanks to a brand new dialog that allows users to open files protected by a password.
> * The DublinCore XMP metadata format is now synchronized with their PDF equivalents.
> * Updated translations, including French, Basque, Russian, Italian, Occitan and Spanish. Thanks to rene-coty, Sergio Varela, Ser82-png, albanobattistella and Mejans for their contributions!
> * Appdata improvements by Sabri Ünal
> 
> Get the latest release from [Flathub](https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor)!
> ![](AUdumycYybeItVtxaiuHkzrX.png)

# Third Party Projects

[SHuRiKeN](https://matrix.to/#/@shuriken:fedora.im) says

> Eeman app is now [available on Flathub](https://flathub.org/apps/sh.shuriken.Eeman), made using GTK 4 and Libadwaita, this app lets you track and get notified of your Salah (prayer) timings, and lets you read the beautiful Quran.

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) says

> A new version of [Dosage](https://flathub.org/apps/io.github.diegopvlk.Dosage) app to Keep track of your treatments was released  this week
> 
> this version (1.5.1) has many improvements, the most important of which are:
> 
> • It's now possible to edit history entries
> • New preference to auto-clear history
> • New row style for select frequency
> • New row style for select date
> • New badge style for history confirmed item
> ![](NCfVrFusZzNjpetgPsfGQHrE.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

Manage your personal finances.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2024.2.0](https://github.com/NickvisionApps/Denaro/releases/tag/2024.2.0) was released this week. 
> This release contains some bug fixes to make your experience more stable!
> 
> Here's the changelog:
> * Improved importing of QIF files
> * Fixed a bug where the app would crash when filtering transactions for certain dates (mainly leap years like this year 😅)
> * Updated and added translations (Thanks to everyone on Weblate)!
> ![](vdTQqIANlnvMWkYKcFPxmgls.png)

# Miscellaneous

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> The [feature freeze for GNOME 46](https://release.gnome.org/calendar/#freezes) is closing in. Starting Feb 10 no changes to UI, features, and APIs are allowed without [approval from the release team](https://handbook.gnome.org/release-planning/freezes.html).

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) reports

> This week has been very busy for the Foundation staff. I started the beginning of the week meeting with our bookkeepers and discussing our books as well as our budget. Midweek was spent travelling to Brussels, where our Executive Director Holly and I spent yesterday meeting with the GNOME Board of Directors and today meeting with our Advisory Board. This weekend is FOSDEM, where I am looking forward to seeing folks stopping by our booth as well as at GNOME Beers Saturday night. I've already had a lot of very productive conversations here in Brussels and am sure there will be many more to come.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

