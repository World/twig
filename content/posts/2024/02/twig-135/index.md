---
title: "#135 Experimental Maps"
author: Felix
date: 2024-02-16
tags: ["apostrophe", "blueprint-compiler", "gnome-maps", "newsflash"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 09 to February 16.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> **Accessibility**
> 
> * Joanie worked on De-PyAtSpi-ing Orca
>     - AtspiDocument: done
>         - Create AT-SPI2 based utilities for the document interface.: https://gitlab.gnome.org/GNOME/orca/-/commit/f36310b6e
>         - Use AXDocument in chromium, gecko, and thunderbird scripts + bookmarks: https://gitlab.gnome.org/GNOME/orca/-/commit/ebe677317
>         - Use AXDocument in the web script: https://gitlab.gnome.org/GNOME/orca/-/commit/7a3dc4871
>     - AtspiComponent: done
>         - Add an AT-SPI2 grab_focus function to AXObject and use it instead of pyatspi: https://gitlab.gnome.org/GNOME/orca/-/commit/a43dfffa3
>         - Create AT-SPI2 component interface utilities: https://gitlab.gnome.org/GNOME/orca/-/commit/47c76e406
>         - Use AXComponent in the event synthesizer: https://gitlab.gnome.org/GNOME/orca/-/commit/1e004fe98
>         - Use AXComponent in more places: https://gitlab.gnome.org/GNOME/orca/-/commit/a0602b147
>         - Begin using AXComponent in scripts and Where Am I Presenter: https://gitlab.gnome.org/GNOME/orca/-/commit/2e595ce6a
>         - Use AXComponent in script utilities and flat review: https://gitlab.gnome.org/GNOME/orca/-/commit/b6a87ff05
>         - Use AXComponent in a couple of places missed before: https://gitlab.gnome.org/GNOME/orca/-/commit/c1761de2a
>         - Remove code obsoleted by AXComponent: https://gitlab.gnome.org/GNOME/orca/-/commit/1573c7d37
>     - AtspiText: WIP
>         - Last (known) use of pyatspi in Orca
>         - Orca should be pyatspi-free when Orca v46 is released 🎉
> * Joanie removed some hacks from Orca
>     - Remove broken _adjustPositionForObj hack: https://gitlab.gnome.org/GNOME/orca/-/commit/1654dfa69
>     - Remove the isShowingAndVisible hack: https://gitlab.gnome.org/GNOME/orca/-/commit/e857c5361
>     - Remove isZombie; use AXObject.is_valid instead: https://gitlab.gnome.org/GNOME/orca/-/commit/5c3ef549f
>     - Web: Eliminate (nearly all of) our text sanity-checks and hacks: https://gitlab.gnome.org/GNOME/orca/-/commit/0aab0e5da
> * Sam fixed some minor things in GNOME Shell
>     - Style papercut fixes before freeze: https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3165
>     - Fixed the Screen Reader regression in Shell's Alt-Tab interface: https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3127
> * Andy continued work in integration Spiel with Orca
>     - https://github.com/eeejay/spiel-demos/pull/1
>     - https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182
> 
> **New Accessibility Stack (Newton)**
> 
> * Matt is working on a client library for Orca and other ATs
> 
> **Platform**
> 
> * Andy fixed some papercuts withthe gnome-online-accounts settings panel UI: https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2891
> * Julian worked on making notifications in the calendar popover expandable and worked with Sam on refreshing the style https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3173
> * Andy, Tobias, and Sam worked on fixing papercuts with the gnome-online-accounts GTK4 port
>     - https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/162
>     - https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/164
>     - https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/175
> * We are investigating a solution for a UX regression AdwDialog https://gitlab.gnome.org/GNOME/libadwaita/-/issues/797
> * Evan landed async support on gobject introspection https://gitlab.gnome.org/GNOME/gobject-introspection/-/merge_requests/404
>     - Async API support in GLib is deferred to next release https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3746
>     - Looking at how to update meson in other projects to use the new GLib GI compiler instead of the old one without async support.
> * Evan is working on finishing merging the gi.ts and ts-for-gir codebases for GI-driven TypeScript bindings. MR here: https://github.com/gjsify/ts-for-gir/pull/144
> * Sonny landed AdwDialog support in Blueprint https://gitlab.gnome.org/jwestman/blueprint-compiler/-/merge_requests/177
> * Andy landed GTK4 port of GNOME Online Account
>     - https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/142
>     - https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2039
> * Andy landed migratation of existing WebDAV accounts in GOA https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/146
> 
> **Hardware Support**
> * Jonas worked on fractional scaling on XWayland https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3567
> * Jonas opened a mutter MR for fractional scaling on XWayland https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3567
> * Jonas landed screencast pipeline blocklisting (https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2976)
>     - we are very close to getting the HW encoder support merged https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2080
> * Dor worked on variable refresh rate support, which is still under review https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1154
> * Dor implemented small refinements to the Settings UI for VRR https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/734
> * Dor added support for showing the VRR range of monitors in the Settings UI when possible:
>     - Mutter: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3576
>     - Settings: https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2260
> * Sam and Dor continued to iterate on the design for VRR in display settings
>     - adjusted the design for the proposed list rows: https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2523#note_2005469
> 
> **Security**
> * Dhanuka continued work on implementing the oo7-daemon: https://github.com/bilelmoussaoui/oo7/pull/56
>     - Fixed most of the issues (a few remains) flagged by Bilal's review.
>     - Fixed some clippy warnings found in oo7 client when the unstable feature is on: https://github.com/bilelmoussaoui/oo7/pull/67
>     - Updated oo7::portal::Keyring::create_item() to return portal::Item: https://github.com/bilelmoussaoui/oo7/pull/68
>     - Updated oo7::portal::Keyring API's "attributes" parameter: https://github.com/bilelmoussaoui/oo7/pull/69
> 
> **Wayland & Portal APIs**
> * Hubert worked on a number of flatpak/portal related things:
>     - libportal settings support
>         - Fixed test so the app fits on smaller screens: https://github.com/flatpak/libportal/pull/142
>         - Settings API (in review) https://github.com/flatpak/libportal/pull/143
>     - flatpak device support:
>         - Proposed and started implementing a compatibility mechanism: https://github.com/flatpak/flatpak/issues/5681
>         - Running the test with ASAN leak all over the place: PR https://github.com/flatpak/flatpak/pull/5683

# GNOME Core Apps and Libraries

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[James Westman](https://matrix.to/#/@flyingpimonster:matrix.org) reports

> Maps' "Experimental Map" mode has a new look for GNOME 46! Features include:
> * Dark mode
> * Translated labels, where available
> * Large Text accessibility setting
> * Symbols for highway routes
> * Adwaita icons
> 
> You can read more about the new style in [my blog post](https://www.jwestman.net/2024/02/10/new-look-for-gnome-maps.html).
> 
> Also in experimental mode, you can now click a label and the info bubble appears immediately, no need to right click and choose "What's Here?".
> ![](DSiZrBiZmwbusylGjNriHCgl.png)

# GNOME Circle Apps and Libraries

[zeenix](https://matrix.to/#/@zeenix:gnome.org) says

> [zbus 4.0](https://github.com/dbus2/zbus/releases/tag/zbus-4.0.0) released. [zbus](https://github.com/dbus2/zbus) is a pure Rust D-Bus crate that many GNOME apps and components rely on. The new version brings a more ergonomic and safer API.

### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> Newsflash 3.1 was released. It brings quality of life improvements and bugfixes.
> 
> https://blogs.gnome.org/jangernert/2024/02/12/newsflash-3-1/

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) says

> Apostrophe's bottombar has seen some improvements. Now the statistics button adapts to the available space, and a lot of under-the-hood changes have been made to the whole bottombar widget
> {{< video src="01aeb55147c00d28d5741b62d810a5dd1515ae131757067533145341952.mp4" >}}

# Third Party Projects

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) reports

> This week, [Flashcards](https://github.com/david-swift/Flashcards) app has introduced some exciting changes that enhance user experience and organization:
> 
> * Tags: Now, users can group flashcards within a set using tags. This feature streamlines organization and makes it easier to manage related cards.
> * Keywords: The app also introduces keywords, allowing users to group entire sets. These keywords serve as filters in the sidebar, making it effortless to locate specific sets.
> ![](CacfiCkkfVoIrMAWQbosyRCk.png)
> ![](gSbsdnSTeyjJHxhKRtMABpKT.png)

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) says

> Love is in the air for privacy enthusiasts and individuals residing in tyrannical states. As a Valentine's Day gift version 4.5.0 of Carburetor released with support for Snowflake and WebTunnel bridges, providing you with even more opportunities to seamlessly connect to the TOR network.
> 
> Built upon Libadwaita, Carburetor allows you to easily set up a TOR proxy on your session without getting your hands dirty with system configs. Initially designed to simplify the lives of GNOME enthusiasts on their mobile phones, it’s now fully usable with mouse and/or keyboard too.
> ![](c289f6c7456a19a2cfa4f343e6f81871f258d0381758173128904671232.png)

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) says

> A new update for [Done](https://flathub.org/apps/dev.edfloreshz.Done),The ultimate task management solution for seamless organization and efficiency, is now available! This update includes a new design, a moved services bar, and the ability to expand task details from within each task.
> ![](vimEkQhqTIwxEScoWxexgcfb.png)
> ![](CtHJBZhSesjLfbUdHCiLjTOJ.png)

[Dexter Reed](https://matrix.to/#/@sungsphinx:matrix.org) announces

> Chats (aka Chatty) has been ported to use the modern libadwaita 1.4 widgets and sidebar by Chris Talbot, with some help from me (sungsphinx) and Bart Gravendeel (aka Monster).
> 
> You can view the pull request here: https://gitlab.gnome.org/World/Chatty/-/merge_requests/1317
> ![](dfavaDXvtnRpGmqVIilPPPOm.png)
> ![](TmVBBkcLyEGipfgbFHGgbSMr.png)

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> I am happy to announce Cambalache's Gtk4 port has a beta release!
> 
> Version 0.17.2 features minors improvements and a brand new UI ported to Gtk 4!
> 
> Available in flathub beta!
> 
> More information at https://blogs.gnome.org/xjuan/2024/02/16/cambalache-gtk4-port-goes-beta/
> ![](9bed3ae769724d8fe1298158ce41380831a3950e1758500351943639040.png)

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> `AdwAlertDialog` support was added to [Blueprint](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/).
> Blueprint is able to support new widgets automatically thanks to GObject Introspection but some widgets have custom builder syntax that needs to be added to Blueprint. Here is the merge request if you'd like to see how custom builder syntax works in Blueprint https://gitlab.gnome.org/jwestman/blueprint-compiler/-/merge_requests/177

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

