---
title: "#161 End-to-End Tests"
author: Felix
date: 2024-08-16
tags: ["keypunch", "snoop", "phosh"]
categories: ["twig"]
draft: false
images:
  - posts/2024/08/twig-161/yvMgiUQvuKttipgIqYVITYlr.png
---

Update on what happened across the GNOME project in the week from August 09 to August 16.<!--more-->

# Sovereign Tech Fund

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> At GNOME we have been using openQA with GNOME OS to run end-to-end tests for different components of our stack. These tests have proven useful, but come with some costs.
> 
> To tackle these costs, the team at Codethink has [leveraged](https://www.codethink.co.uk/articles/2024/A-new-way-to-develop-on-Linux-PartII/) its previous work with [sysext-utils](https://gitlab.gnome.org/tchx84/sysext-utils) and systemd's system extensions. As a result, we came up with a workflow that integrates system extensions with development and testing pipelines. This provides an improved workflow for reviewers and testers.
> 
> New CI/CD [components](https://gitlab.gnome.org/GNOME/citemplates/) are now available to all GNOME modules, and we have integrated some of these in mutter and gnome-shell pipelines already.
> 
> This project was a collaboration between Codethink and the GNOME Foundation, through the Sovereign Tech Fund (STF).

# GNOME Core Apps and Libraries

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [GDM](https://gitlab.gnome.org/GNOME/gdm/) and [gnome-session](https://gitlab.gnome.org/GNOME/gnome-session) can now be built without X11 support, following the steps of Mutter and GNOME Shell.

# GNOME Circle Apps and Libraries

[Hugo Olabera](https://matrix.to/#/@hugolabe:matrix.org) announces

> Wike, the Wikipedia reader for GNOME, has been updated to version 3.1.0. The most visible changes have to do with the migration of windows and dialogs to the new responsive widgets from libadwaita.
> 
> Two new languages ​​have been added to the UI translations (Simplified Chinese and Hungarian), and many of the existing translations have been updated.
> 
> The user folder access permission in the Flatpak sandbox has been removed, which is no longer needed thanks to the recent changes implemented in Webkit.
> 
> Finally, the runtime version has been updated to GNOME 46 and features the usual set of minor tweaks and bug fixes.
> ![](yvMgiUQvuKttipgIqYVITYlr.png)

# Third Party Projects

### Snoop [↗](https://gitlab.gnome.org/philippun1/snoop)

Snoop through your files.

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> [Snoop 0.4](https://gitlab.gnome.org/philippun1/snoop/-/releases/0.4) offers a variety of little fixes and tweaks.
> 
> The search path and search string will now be checked for validity and a warning indicator is shown if a path is invalid or the search string is empty.
> 
> Paths containing a ~ or a space are now handled properly.
> 
> If you have installed the nautilus extension from the settings menu, there is now also a Remove button to uninstall it.
> ![](kQVDmyKBxzSPVpcxoxpZWcYu.png)
> ![](vnhTbOoaWduQrFTtbLCxjTze.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> Phosh 0.41.0 is out:
> 
> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) gained a new Quick Setting to toggle Wi-Fi Hotspot on/off and a Bluetooth status page. The media-player widget now displays track position/ length and got a progress bar. Squeekboard got new layouts and you can now put the phone into silent mode when pressing Vol- on incoming calls.
> 
> There's more. Check the full details [here](https://phosh.mobi/releases/rel-0.41.0/)
> {{< video src="sjPLGNFzXOrpnjfsrkSgrePr.mp4" >}}

### Keypunch [↗](https://github.com/bragefuglseth/keypunch)

Practice your typing skills

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> Ciao! Keypunch 3.1 is out, and it speaks Italian! This release also brings text generation support for Czech, Occitan and Polish, and UI translations for Occitan and Polish.
> 
> Thanks to everyone who has requested for their languages to be added and patiently tested the text generation implementations; keep the requests coming!
> 
> Get Keypunch on Flathub: https://flathub.org/apps/dev.bragefuglseth.Keypunch
> ![](1da5c68832b01013b8a319eeafc29d5dfadb0ae21824507622510821376.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

