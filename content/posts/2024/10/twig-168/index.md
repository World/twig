---
title: "#168 Testing Portals"
author: Felix
date: 2024-10-04
tags: ["gnome-software", "dejadup", "flatseal", "identity", "phosh", "glib", "parabolic", "video-trimmer"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 27 to October 04.<!--more-->

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Julian Sparber is tackling adding tests for some tricky-to-test portal code in GLib 🙌 https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4176 — as always, adding tests has paid itself back by finding bugs, which he’s fixed

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Automeris Naranja and Hari Rana have continued work this week to modernise GNOME Software’s UI with libadwaita 1.6 widgets, while Sid is working on making submission of app reviews not block the UI.
> ![](yKejRqaHoYcJiZEzSnunbQEp.png)

# GNOME Circle Apps and Libraries

### Video Trimmer [↗](https://gitlab.gnome.org/YaLTeR/video-trimmer)

Trim videos quickly.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> [Video Trimmer](https://flathub.org/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer) v0.9 is out with support for accent colors and improvements to the hotkey behavior.
> ![](edf4c1936416b5ab72fad5f904de323e312bca9b1841929310949081088.png)

### Identity [↗](https://gitlab.gnome.org/YaLTeR/identity)

Compare images and videos.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> For the new [Identity](https://flathub.org/apps/org.gnome.gitlab.YaLTeR.Identity) v0.7 release, I implemented image loading via the [glycin](https://gitlab.gnome.org/sophie-h/glycin) library. It lets you open newer image formats like AVIF and JPEG XL, and improves rendering for other images, like using the correct color spaces for regular JPEGs.
> ![](dce4953f81839d6dc54547e4087cc8a0fc6493051841927982919188480.png)

### Déjà Dup Backups [↗](https://apps.gnome.org/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) reports

> [Déjà Dup](https://flathub.org/apps/org.gnome.DejaDup) 47 is now available! This release features Restic support improvements and the latest adwaita widgets and accent colors.

# Third Party Projects

[Vladimir Kosolapov](https://matrix.to/#/@vmkspv:matrix.org) says

> This week I released my first app — [Netsleuth](https://github.com/vmkspv/netsleuth).
> 
> It’s a simple utility for the calculation and analysis of IP subnet values, designed to simplify network configuration tasks. There's no longer a need to rely on various ad-laden websites with awkward designs, as Netsleuth is native and available offline.
> 
> Check it out on [Flathub](https://flathub.org/apps/io.github.vmkspv.netsleuth)!
> ![](UcREQJrjLyuDlRePlagbQgOj.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) 0.42.0 is out:
> 
> The lock screen now adjusts to smaller resolutions and we fixed a whole lot of bugs around (but not limited to) OSK text input when using text prediction - up to a point where we now enable it by default when the app requests it (and the OSK supports it). [squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard) saw another round of layout improvements and additions.
> 
> Check the full details [here](https://phosh.mobi/releases/rel-0.42.0/)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2024.10.0](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.10.0) is here!
> 
> This update features a brand new rewrite of Parabolic in C++. Users should now have a faster and more stable downloading experience, with the continued reliability and customizability loved by our users.
> 
> We have also redesigned the user interfaces on both the GNOME and Windows platforms to make it easier to find the Parabolic features you love and want to use. We have also refined and improved the options available when configuring individual downloads, playlists, and Parabolic as a whole. Besides new things, we have fixed tons of bugs throughout the downloading backend that users have been waiting for.
> 
> We hope you all love this release as much as we loved making it! A huge thank you to all users who worked with us in fixing issues, testing the betas, contributing translations and more ❤️
> 
> Here's the full changelog:
> * Parabolic has been rewritten in C++ for faster performance
> * The Keyring module was rewritten. As a result, all keyrings have been reset and will need to be reconfigured
> * Audio languages with audio description are now correctly recognized and handled separately from audio languages without audio description
> * Audio download qualities will now list audio bitrates for the user to choose from
> * Playlist downloads will now be saved in a subdirectory with the playlist's title within the chosen save folder
> * When viewing the log of a download, the command used to run the download can now also be copied to the clipboard
> * The length of the kept download history can now be changed in Preferences
> * On non-sandbox platforms, a browser can be selected for Parabolic to fetch cookies from instead of selecting a txt file in Preferences
> * Added an option in Preferences to allow for immediate download after a URL is validated
> * Added an option in Preferences to pick a preferred video codec for when downloading video media
> * Fixed validation issues with various sites
> * Fixed an issue where a specified video password was not being used
> * Redesigned user interface
> * Updated yt-dlp
> ![](KybkdPqbKYKDPHRZonmoQZXI.png)
> ![](DkKMCmMjyewjPwUFKXMKLuXs.png)

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> [Flatseal](https://flathub.org/apps/com.github.tchx84.Flatseal) 2.3.0 is out! This new release has caught up with new Flatpak permissions, added Hindi translation thanks to Scrambled777, and updated its runtime to GNOME 47. Make sure to keep your GNOME runtime up-to-date to prevent some common [issues](https://github.com/tchx84/Flatseal/issues/726)!

# Shell Extensions

[Marcin Jahn](https://matrix.to/#/@mnjahn:matrix.org) reports

> (Almost) all of my Gnome extensions do support Gnome 47 now. What's even better is that all the updates came from contributors, so all I had to do was to merge a couple of PRs. Thanks!
> 
> * [Do Not Disturb While Screen Sharing Or Recording](https://github.com/marcinjahn/gnome-do-not-disturb-while-screen-sharing-or-recording-extension)
> * [Quick Settings Audio Devices Hider](https://github.com/marcinjahn/gnome-quicksettings-audio-devices-hider-extension)
> * [Quick Settings Audio Devices Renamer](https://github.com/marcinjahn/gnome-quicksettings-audio-devices-renamer-extension)
> * [Dim Completed Calendar Events](https://github.com/marcinjahn/gnome-dim-completed-calendar-events-extension)

# Events

[Deepesha Burse](https://matrix.to/#/@deepeshaburse:matrix.org) announces

> GNOME Asia 2024 is happening in Bengaluru, India between 6th to 8th of December. We're happy to announce that the call for proposals is now open and will close on October 8th, 2024. This is your chance to submit a proposal and speak at the conference!
> 
> You can submit your CFPs at: https://events.gnome.org/event/258/abstracts/
> and find more info at: https://foundation.gnome.org/2024/09/12/gnome-asia-2024-in-bengaluru-india/

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) says

> This weekend, from Oct 4 to Oct 5, the [Linux App Summit](https://linuxappsummit.org/) is taking place in Monterrey, Mexico. LAS is dedicated to collaboration on all aspects aimed at accelerating the growth of the Linux application ecosystem. 
> 
> To attend remotely, [register here](https://linuxappsummit.org/register/). Videos from LAS will also be live streamed on the [LAS YouTube channel](https://www.youtube.com/channel/UCjSsbz2TDxIxBEarbDzNQ4w).
> ![](8c799b32a98272b7919a0272710c280d628c8e8d1842208378336575488.png)

# GNOME Foundation

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) says

> The last months, the GNOME Foundation Board of directors has been busy with budgeting and other strategic decisions, and we've written a small update to keep everybody informed: https://discourse.gnome.org/t/update-from-the-board-2024-10/24346 We will follow-up with a more detailed discussion of the budget in the following weeks

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
