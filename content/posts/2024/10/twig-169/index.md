---
title: "#169 Wrapped Boxes"
author: Felix
date: 2024-10-11
tags: ["libadwaita", "hieroglyphic", "gaphor"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 04 to October 11.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> libadwaita got another new widget - [`AdwWrapBox`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.WrapBox.html) - similar to `GtkBox`, but wrapping children when they can't fit onto the same line. This can be useful for e.g. displaying tag pills
> ![](f667cd3295f967fa96b0138310654037798a36741843679746462842880.png)

# GNOME Circle Apps and Libraries

### Hieroglyphic [↗](https://github.com/FineFindus/Hieroglyphic)

Find LaTeX symbols

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> A new Hieroglyphic update has just been released. The classification backend is now powered by a machine learning model trained on the previously used classification data, improving classification speed and accuracy. However, due to a lack of good training data, some symbols are now less accurately classified.
> To improve the training data/accuracy, users can now optionally submit their recognized symbols.
> ![](IEaQMofsLUQaZAcDcQwMofdR.png)

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) announces

> This week @danyeaw released Gaphor 2.27.0.
> 
> Highlights of this release include
> lots of small improvements and usability fixes:
> 
> * Export all diagrams easily from the GUI
> * New elements: Trigger and Actions for state machine transitions
> * Improved Picture selection and provide a default name
> * A macOS ARM (Apple Silicon) version
> * Flatpak version uses GNOME 47
> ![](zeVEvvXHHDARFcrqOJFiGiNm.png)

# Events

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) announces

> the GNOME Foundation is looking for volunteers to help organize and be at the GNOME stand during FOSDEM: https://discourse.gnome.org/t/call-for-help-for-a-fosdem-stand/24432 If you want to help with Outreach, this is your opportunity! If we don't get enough people to sign up, we will not be able to apply to the stand

# GNOME Foundation

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> The GNOME Foundation Board published its minutes for the past four months this week. These can be found in two Discourse posts, [one for June](https://discourse.gnome.org/t/foundation-board-minutes-for-2024-06-24-2024-06-13-2024-06-06/24405), and the [other for July–September](https://discourse.gnome.org/t/foundation-board-minutes-for-july-september-2024/24466). This means that the board is now up to date with publishing its minutes.
> The board also published its board responsibilities, ethical conduct, and confidentiality policies for the first time. These were included in a new [policies wiki page](https://gitlab.gnome.org/Teams/Board/-/wikis/Policies), which was created as part of the board's migration off [wiki.gnome.org](https://wiki.gnome.org/) (which is planned for retirement).

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) says

> as promised in our communication last week, the GNOME Foundation Board of Directors has written an update on the financial status and budget approved between October 2024 and September 2025: https://discourse.gnome.org/t/foundation-2024-2025-budget-and-economic-review/24436. We hope to continue providing regular updates on financial work

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
