---
title: "#170 Portal Updates"
author: Felix
date: 2024-10-18
tags: ["tracker", "webkitgtk", "shortwave", "fractal", "mutter", "apostrophe", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 11 to October 18.<!--more-->

# Sovereign Tech Fund

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> The development version of Flatpak has received support for listing enumerable and blocked USB devices. This is the first step towards a fully featured USB portal, which is in the works.
> 
> Flatpak itself doesn't expose nor blocks USB devices, but it provides the metadata for the USB portal to allow or deny an app's access to particular devices.
> 
> With this mechanism, apps can request access to devices without opening holes in the sandbox (in this case, the sandbox hole is quite big; apps that access USB devices need to give access to *all* devices on the system).
> 
> In addition to that, app store reviewers can review ahead of time which USB permissions the apps will be requesting, which may add an extra layer of security and trust to Flatpak-based app stores like Flathub.
> 
> This work was only made possible thanks to Sovereign Tech Fund, various community members that helped reviewing and shaping up the feature, and Hub for pushing it to the finish line!

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> The Notifications portal received a major update, and now supports a variety of new features such as notification sounds, markup text in the notification body, and more.
> 
> A lot of it is described in [this article on the Mutter & GNOME Shell blog](https://blogs.gnome.org/shell-dev/2024/04/23/notifications-46-and-beyond/). The grand plan is to unify notifications under the Notification portal.
> 
> Thanks to Julian Sparber (he/him) for working on this, and for Sovereign Tech Fund for funding this work.
> ![](cb487c9960876108844c3057748ce3711ac4c3891847341631745818624.png)

# GNOME Core Apps and Libraries

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> WebKitGTK is now able to report the accessibility contents of web pages even when running under a Flatpak sandbox, closing a major gap in accessibility on the Linux desktop.
> 
> Currently this requires unreleased versions of WebKitGTK and Flatpak, but releases are on the way.
> 
> This is hard to capture in a screenshot but here's real proof of GNOME Web running as a Flatpak with the accessible contents available.
> 
> A detailed blog post about this work, its challenges, and how this was achieved, is being written.
> ![](1444e33c2732b9be949ff7c9a1a33c19695b06fa1847335717630377984.png)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Cogitri](https://matrix.to/#/@cogitri:gnome.org) announces

> There's finally a new release for tracker-rs, making it compatible with the newest gtk-rs libraries. This way, anyone using the latest gtk-rs can now use Tracker as their database backend :)

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Gert-dev](https://matrix.to/#/@gert-dev:matrix.org) reports

> A few significant performance improvements for users with monitors directly attached to secondary GPUs common in high-end laptops are currently being finalized during review and in testing for mutter:
> * [Avoid CPU-side stalls with NVIDIA secondary GPUs with monitors directly attached to it](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4015)
> * [Improve frame rate on monitors attached to secondary GPUs in copy mode](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4027)
> * [Pass damage rectangles to secondary GPUs in the copy path](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4073)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Chun-wei Fan (with some collaboration from Christoph Reiter) has added a CI job for building GLib on Windows on ARM64, which should help us catch regressions on that platform. See https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4342. If you’re interested in getting GLib (and the GNOME platform) to work better on Windows/ARM64 please get in touch 😀

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

> Clicking on search results at duckduckgo.com in Web (Epiphany) currently gives an error message. This seems to be an issue on DuckDuckGo's side.
> 
> The Web maintainers are trying to resolve the issue with DuckDuckGo. Meanwhile, it is possible to work around this issue by opening DuckDuckGo's settings and disabling the "Redirect (when necessary)" option in the "Privacy" category.
>
> See [!1654](https://gitlab.gnome.org/GNOME/epiphany/-/merge_requests/1654) for more details. 


# GNOME Circle Apps and Libraries

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 30000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> It happened! A new version of your favourite internet radio player is now available!
> Shortwave 4.0 brings a modernised user interface, a couple of new features, performance improvements, bug fixes - and much more!
> 
> Check out the blog post for more details:
> https://blogs.gnome.org/haeckerfelix/2024/10/18/shortwave-4-0/
> {{< video src="VZMFObKhereCZNWtSeyLNceW.webm" >}}

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu (he/they/she)](https://matrix.to/#/@somas95:gnome.org) announces

> I've been working on reimplementing an obscure feature Apostrophe used to have: inline previews of images, LaTeX formulas, footnotes, web links and even word definitions. It was hard to spot as it was only available when you ctrl+clicked on text, but now the preview is also available through the context menu. Performance has also been greatly improved
> {{< video src="0b80e68a8cf2a7faa76ee2adfd82bfd71c6dd17c1846280143987802112.mp4" >}}

# Third Party Projects

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> It's time to experience _La vie en rose_! We just released Fractal 9.rc and to show our support 🩷 for Breast Cancer Awareness Month, we decided to change the accent color to pink. And it is also packed with bug fixes, take a look for yourself:
> 
> * We used to only rely on the secrets provider to tell us which Matrix accounts are logged-in, which caused issues for people sharing their secrets between devices. Now we also make sure that there is a data folder for a given session before trying to restore it.
> * Our notifications are categorized as coming from an instant messenger, so graphical shells that support it, such as Phosh, can play a sound for them.
> * Some room settings are hidden for direct chats, because it does not make sense to change them in this type of room.
> * The size of the headerbar would change depending on whether the room has a topic or not. This will not happen anymore.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, it should be mostly stable and we expect to only include minor improvements until the release of Fractal 9.
> 
> If you are wondering what to do on a rainy day, you can try to fix one of our [newcomers issues](https://gitlab.gnome.org/World/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers). We are always looking for new contributors!

# GNOME Websites

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> A list of all [components of the GNOME project](https://developer.gnome.org/components/) is now available on the development page. Components encompass everything from the [Shell](https://developer.gnome.org/components/#gnome-shell), over the [icon theme](https://developer.gnome.org/components/#adwaita-icon-theme), to libraries like [GTK](https://developer.gnome.org/components/#gtk). The page is listing GNOME Core components as well as GNOME Circle components. 
> 
> All the information about the components on the page is automatically collected from the projects `.doap`-files. You can help by updating or adding information to the projects and providing avatars for projects that don't have one yet. The page's [readme](https://gitlab.gnome.org/Teams/Websites/developer.gnome.org-components#gnome-components) has some more info on this.
> ![](4633ced9a8cc8701938654a24541f42698781fa61847340818826788864.png)

# Shell Extensions

[Martin (he/him)](https://matrix.to/#/@xmkhl:matrix.org) announces

> I released the [Window Desaturation extension](https://extensions.gnome.org/extension/7423/window-desaturation/), which desaturates background windows, leaving the focused window "highlighted" by having all it's color.

# Miscellaneous

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> XDG Desktop Portal 1.19.0 has been released! It's the first development snapshot of what will become the 1.20 release series. The highlights of this release are:
> 
> * Many improvements to how the `portals.conf` config files are parsed. This includes respecting the order of portal backends specified for each key, having default fallbacks, and handling various edge cases better.
> * Files that are exposed through the Document portal now report their real path as the `user.document-portal.host-path` xattr property. Alternatively, apps can query the real path of a file using the new `GetHostPath()` D-Bus method of the Document portal. Apps that display a file path in its user interface are encouraged to use the real path.
> * Implement `getlk` and `setlk`, and honour `O_NOFOLLOW`, in the Document portal's FUSE filesystem. This should make SQLite backups work on sandboxed apps, and improve the reliability of the Document portal in general.
> * Make the Background portal more robust when validating autostart files.
> * Massively improved documentation and website
> * Many, *many* leaks were fixed.
> 
> You can find the full release notes [here](https://github.com/flatpak/xdg-desktop-portal/releases/tag/1.19.0).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
