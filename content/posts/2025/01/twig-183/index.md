---
title: "#183 Updated Flatpak"
author: Felix
date: 2025-01-17
tags: ["fractal", "parabolic", "gnome-podcasts", "gjs", "libadwaita", "gnome-control-center", "gnome-mahjongg", "gnome-maps"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 10 to January 17.<!--more-->

### Flatpak [↗](https://flatpak.org/)

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> ![](ce2fb374d0472d13e95e5b0e9b77655528b485ea1879154812960899072.png)
>
> Last week, the Flatpak 1.16.0 was released. It's the first stable release in years! A lot has happened in the meantime, some of the highlights are:
> 
>  * Listing USB devices, which in combination with the USB portal, allow for sandboxed device access
>  * Accessibility improvements
>  * Support for Wayland security context
>  * ... and more!
> 
> I've written about it in more detail in a blog post: https://feaneron.com/2025/01/14/flatpak-1-16-is-out/

# GNOME Core Apps and Libraries

[Maximiliano 🥑](https://matrix.to/#/@msandova:gnome.org) announces

> [Snapshot](https://gitlab.gnome.org/GNOME/snapshot) 48.alpha was just released. In this release we added support for reading QR codes. 
> 
> The aperture library also gained this feature and uses the rqrr crate, meaning that it does not require to link against zbar anymore!
> ![](9ca6389fd4829934cb59e543e8daaa2cede7be4a1880346185026240512.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> a few more improvements for libadwaita adaptive preview: the inspector UI is now less confusing and there's now a shortcut that opens it directly (Shift+Ctrl+M). The API for opening it is now [public](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.ApplicationWindow.adaptive-preview.html) and libadwaita demo now has an adaptive preview entry in its menu, along with inspector
> ![](4013fb24266e5124a7ec74ca37b4d8f0d9d603831879936534208053248.png)

### Maps [↗](https://apps.gnome.org/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) says

> Maps now has a re-designed user location marker, using a new "torch" to indicate heading, and using the system accent color
> ![](CDAevHEKyLWzaQmCnfpkghVQ.png)
> ![](lTCBIxsQErsaPWuMmHPZDrMr.png)

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Screen Time support has landed in the Wellbeing panel in GNOME Settings, which completes the round of merge requests needed to get that feature working across the desktop. It allows you to monitor your daily usage of the computer, and set a time limit each day. This is in addition to break reminders, which landed late last year.
> 
> Big thanks to Florian Müllner and Matthijs Velsink for their reviews of the work, and to Sam Hewitt and Allan Day for design work on the feature.
> 
> It’s now available to test in GNOME OS nightly images. If you find bugs in the feature, please file an issue against either gnome-control-center or gnome-shell, and label it with the ‘Wellbeing’ label.
> ![](npBOPSwRjiPmoifGmdQsJUXd.png)

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) reports

> We also landed several improvements from Marco Trevisan that further improve performance in accessing GObject properties, like `button.iconName` or `label.useMarkup`, and make GObject methods use less memory when called from JS.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GJS, the command-line debugger can now examine private fields of objects, thanks to Gary Li.

# GNOME Circle Apps and Libraries



### Podcasts [↗](https://gitlab.gnome.org/World/podcasts)

Podcast app for GNOME.

[alatiera](https://matrix.to/#/@alatiera:matrix.org) says

> New year, New release 🎉!
> 
> This release brings lots of small improvements to make
> everything a little bit better!
> 
> The following are now possible:
> * You can now mark individual episodes as played
> * The Shows will now scale based on the window size
> * You can close the window with the Control + W shortcut
> 
> While we also changed some internal things
> * Rework the download machinery to be faster and more efficient
> * Improved application startup times
> 
> And fixes a couple of pesky bugs
> * Automatically detect the image format for thumbnails
> * Dates are now displayed and calculated using localtime instead of sometimes using UTC
> * Fix accessibility warnings in the Episode Description
> * Correctly trigger a download when thumbnail cover for mpris is missing
> * Correctly calculate the episode download size if its missing from the xml metadata

# Third Party Projects

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Refine version 0.4.0 was released. Refine is a GNOME Tweaks alternative I'm working on which follows the [data-driven](https://en.wikipedia.org/wiki/Data-driven_programming), [object-oriented](https://en.wikipedia.org/wiki/Object-oriented_programming), and [composition](https://en.wikipedia.org/wiki/Object_composition) paradigms. The end goal is to have the convenience to add or remove options without touching a single line of source code.
> 
> Version 0.4.0 exposes the following features from dconf:
> 
> * font hinting and font antialiasing options
> * background options
> * window header bar options
> * resize with secondary clicks toggle
> * window focus mode options
> * automatically raise on hover toggle
> 
> I also released version 0.2.0 before, which introduced a combo row for selecting the preferred GTK 3 and GTK 4 theme.
> 
> You can get Refine on [Flathub](https://flathub.org/apps/page.tesk.Refine).
> ![](289b8e323f27d1e6f774e5554382eefd326fed3a1880342267458224128.png)
> ![](42878efe44fc3d8475e16a156a17eab0f81809811880342195593019392.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Parabolic [V2025.1.2](https://github.com/NickvisionApps/Parabolic/releases/tag/2025.1.2) is here with fixes for various bugs users were experiencing.
> 
> Here's the full changelog:
> *  Fixed an issue where the cookies file was not used when validating media URLs
> *  Fixed an issue where the Qt version of the app did not select the Best format when the previously used format was not available
> *  Fixed an issue where the update button on the Windows app did not work
> *  Updated yt-dlp
> ![](wyLrjJbiGbNMstwafNsXtjzt.png)

### Mahjongg [↗](https://gitlab.gnome.org/GNOME/gnome-mahjongg)

A solitaire version of the classic Eastern tile game.

[Mat](https://matrix.to/#/@mat:mathias.is) reports

> Significant improvements have been made to Mahjongg in the past week:
> 
> * New mode to rotate between layouts when starting a new game (added by K Davis)
> * Added transitions/animations when starting a new game and pausing a game
> * Uses GTK 4's GPU rendering to render tiles, instead of Cairo
> * Re-rendered the 'Smooth' theme for high resolution screens
> * No more delays when starting a new game, thanks to many optimizations
> * Reduced frame drops when resizing the game window
> * Various code cleanups and some fixes for memory leaks
> 
> These changes will be available in Mahjongg 48 later this spring. Until then, you can try them out by installing the `org.gnome.Mahjongg.Devel` Flatpak from the [GNOME Nightly repository](https://nightly.gnome.org/). Enjoy!
> ![](VwGBcvdhzPdCrooJsdqlDAtF.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) announces

> In this cold weather, we hope Fractal 10.rc will warm your hearts. Let’s celebrate this with our own awards ceremony:
> 
> * The most next-gen addition goes to… making Fractal [OIDC aware](https://areweoidcyet.com/#next-gen-auth-aware-clients). This ensures compatibility with [the upcoming authentication changes for matrix.org](https://matrix.org/blog/2025/01/06/authentication-changes/).
> * The most valuable fix goes to… showing consistently pills for users and rooms mentions in the right place instead of seemingly random places, getting rid of one of our oldest and most annoying bug.
> * The most sensible improvement goes to… using the send queue for attachments, ensuring correct order of all messages and improving the visual feedback.
> * The most underrated feature goes to… allowing to react to stickers, fixing a crash in the process.
> * The most obvious tweak goes to… removing the “Open Direct Chat” menu entry from avatar menu and member profile in direct chats.
> * The clearest enhancement goes to… labelling experimental versions in the room upgrade menu as such.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, it should be mostly stable and we expect to only include minor improvements until the release of Fractal 10.
> 
> If you are wondering what to do on a cold day, you can try to fix one of our [newcomers issues](https://gitlab.gnome.org/World/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers). We are always looking for new contributors!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
