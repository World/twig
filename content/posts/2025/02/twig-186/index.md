---
title: "#186 Media Parsing"
author: Felix
date: 2025-02-07
tags: ["gjs", "shortwave"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 31 to February 07.<!--more-->

# GNOME Core Apps and Libraries

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> The LocalSearch filesystem extractor is switching from GStreamer to ffmpeg / libav for media file parsing in the next major release. GStreamer served us well for almost 20 years in this role as a media parsing library, but it was designed for media playback and not fast metadata extraction. It's plugin support meant that it behaves differently depending which plugins are installed on a given system, which makes it impossible to fully test. The last few years saw the LocalSearch metadata extractor gain much better sandboxing, but due to the way GStreamer loads plugins, we had to poke several holes in the sandbox to make it work, and play a whack-a-mole game to blocklist any GStreamer plugins that wouldn't work in the sandbox.
> 
> The new ffmpeg-based implementation is faster, and also safer due to tighter sandboxing. It supports all the media formats we need to parse, and in fact, on most systems GStreamer was already processing many filetypes using libav. Thanks to Carlos Garnacho for the [merge request](https://gitlab.gnome.org/GNOME/localsearch/-/merge_requests/579).

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> In GNOME 48.beta, the GJS interactive console will be asynchronous. You can, for example, create a window with a button, connect a signal handler, click the button, and the signal handler will run when the button is clicked. Previously, the signal handler wouldn't run because it was blocked by the console waiting for input. (This doesn't yet make `await` work in the interactive console, but it is a prerequisite.) Thanks to Evan Welsh for doing the thorough research here!

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> Also in GNOME 48.beta, we have an easier way to create `GObject.Value` thanks to Gary Li. Usually for C APIs that use GValue, GJS transparently substitutes native JS values. However, in some cases you need to use the GObject.Value wrapper in JS. Previously you would create an empty object, call `init` to set the type, and then store a value:
> 
> ```
> const value = new GObject.Value();
> value.init(String);
> value.set_string('a string');
> ```
> 
> Now you can just do it in one: `new GObject.Value(String, 'a string');`


# GNOME Releases

[Jeremy Bicha](https://matrix.to/#/@jbicha:ubuntu.com) reports

> The Debian GNOME team has announced that GNOME 48 will be included in Debian 13 "Trixie". Debian 13 will be released later this year.

# GNOME Circle Apps and Libraries

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 30000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> Shortwave 5.0 is now available, bringing background playback and completely revamped stream recording! 
> 
> For more details, check out the blog post:
> https://blogs.gnome.org/haeckerfelix/2025/02/05/shortwave-5-0/
> ![](TUYlpFGeKxnupDXztMPyLPTJ.png)

# Third Party Projects

[fabrialberio](https://matrix.to/#/@fabrialberio:matrix.org) says

> Pins (formerly PinApp) version 2.0 is now available! The new version is the result of a complete rewrite, switching from Python to C. This brings a lot of major and minor improvements, including a new grid view and support for autostart applications.
> Checkout the app on [Flathub](https://flathub.org/apps/io.github.fabrialberio.pinapp).
> ![](uLzTZRLsjCimkXVoHcuKkdux.png)
> ![](LvLMjbyPYsIMUmCwtjoBpJqU.png)

[petsoi](https://matrix.to/#/@petso:matrix.org) says

> [Words!](https://flathub.org/apps/page.codeberg.petsoi.words) 0.3 is now available with support for multiple dictionaries and different word lengths! I’ve also added a German dictionary. If you'd like to see your language included, feel free to submit a word list.
> ![](mlDMDbIbTtfznjguGgOfehpO.png)
> ![](SVohQFbCcuZDfHHDmhqajBvC.png)

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> The [GNOME Shell 48 extensions porting guide](https://gjs.guide/extensions/upgrading/gnome-shell-48.html) has been released! If you need any help with the port, you can ask us on the [GNOME Extensions Matrix Channel](https://matrix.to/#/#extensions:gnome.org).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
