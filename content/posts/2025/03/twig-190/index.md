---
title: "#190 Cross Platform"
author: Felix
date: 2025-03-07
tags: ["gtk", "apostrophe"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 28 to March 07.<!--more-->

# GNOME Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[sp1rit](https://matrix.to/#/@sp1rit:tchncs.de) announces

> The GTK Android backend [gained preliminary support for OpenGL](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/8278). While *not fully* implemented yet, most applications that make use of Gtk.GLArea should work now and other applications should see noticeable performance improvements, especially on shadows.
> ![](asdfx.png)

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Thanks to [the work](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/8071) of Arjan GTK applications on macOS will use native window controls starting the 4.18 release. To preserve backward compatibility, this behaviour is opt-in; application developers can use native controls by setting the [`GtkHeaderBar:use-native-controls`](https://docs.gtk.org/gtk4/property.HeaderBar.use-native-controls.html) property, either in code or in UI definition files.
> ![](cf573e45394adf81a34afb59d50b515abbe7475c1898060580657299456.png)

# GNOME Circle Apps and Libraries

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu (he/they/she)](https://matrix.to/#/@somas95:gnome.org) says

> I've been spending some time reworking all the regex expressions that Apostrophe uses for its markdown syntax highlighting and document statistics. They're now more accurate and less prone to performance issues. They should be easier to maintain from now on, as I've documented them thoroughly and written new tests. They now adhere more to pandoc's markdown flavour, as it's the default for Apostrophe, instead of commonmark.

# Third Party Projects

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) reports

> Introducing Refine 0.5.0, the GNOME Tweaks alternative leveraging the [data-driven](https://en.wikipedia.org/wiki/Data-driven_programming) and [composition](https://en.wikipedia.org/wiki/Object_composition) paradigms. This version re-adds the Document font option, and renames "Middle Click Paste" to "Middle Click to Paste Text" with an accompanying subtitle.
> 
> 0.5.0 also adds the capability to [rearrange the titlebar's window buttons](https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/merge_requests/34). This new feature also lets you add the minimize and maximize buttons.
> 
> While we thoroughly tested right-to-left (RTL) direction and keyboard navigation with a screen reader, it's worth noting that we're no experts. **We welcome feedback from those who use Refine in RTL and/or with a keyboard and screen reader.**
> 
> You can get Refine 0.5.0 on [Flathub](https://flathub.org/apps/page.tesk.Refine)
> {{< video src="6ee7c94680b786ec5aff428acb99f97b765eb9751897621502220566528.webm" >}}

[fabrialberio](https://matrix.to/#/@fabrialberio:matrix.org) announces

> Pins version 2.1 is now available! With this release the app grid will be more complete, thanks to fixes and improvements in loading apps, and more colorful, since Pins can now display app icons from non-standard locations. I have also added an option to show or hide system apps.
> Checkout the app on Flathub: https://flathub.org/apps/io.github.fabrialberio.pinapp
> ![](KmAnFGGBWqLhOVMaNPXxhMhf.png)

# Shell Extensions

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) reports

> Foresight is a new GNOME Shell Extension that *automagically* opens the activities view on empty workspaces. It uses callbacks to monitor windows and workspaces (instead of actively checking on them on certain time intervals), which makes it very efficient and responsive.
> ![](nIcsZAVscOZiJqqshULtcvdf.png)
> {{< video src="MDLqiYFZMOdTBqAiiJCUduch.mp4" >}}

# Miscellaneous

[Iverson Briones (any)](https://matrix.to/#/@ivercoder:matrix.org) says

> Welcome onboard the GNOME l10n fleet, [Filipino l10n team](https://l10n.gnome.org/teams/fil/)! Filipino, while having over 80 million speakers worldwide, had no localization effort up 'til now. But fret not—you can now join the newly baked Filipino l10n team and translate away. #ItsMoreFunInGNOME🇵🇭
> 
> The **Loupe image viewer** app was the first to be completely localized to Filipino this week—set your system language then grab Loupe's latest [nightly build](https://nightly.gnome.org/repo/appstream/org.gnome.Loupe.Devel.flatpakref) and take a peek! Second in the race is **Audio Sharing** which also sports a complete localization on the latest [nightly build](https://nightly.gnome.org/repo/appstream/de.haeckerfelix.AudioSharing.Devel.flatpakref). Last but not the least, the GNOME UI for XDG desktop portals has also been fully localized. GNOME Software, Weather, and Amberol are next, with their localizations currently being molded in the oven. More apps and components to be 🇵🇭-ized soon!
> 
> P.S. Bisaya speakers, you will be next =)
> ![](TklnJgcpFJbIvGWNiKlKQIbW.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
